# Heat gun

A hot air gun is useful to shrink the heatshrink required for the LED cable, if you are making this up rather than using the LED PCB.  If one is not available, a hairdryer or soldering iron can be used instead - though it is very easy to melt or char the tubing if a soldering iron is used.